import axios from "axios";

const url = `${process.env.NEXT_PUBLIC_STRAPI_API_URL}`;

const strapi = axios.create({
  baseURL: url,
  // auth: {
  //     username: "username",
  //     password: "password",
  // },
  withCredentials: true,
  timeout: 25000,
  headers: {
    "Content-Type": "application/json",
  },
});

export default strapi;
