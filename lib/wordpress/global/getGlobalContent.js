import axios from "axios";

export const querySiteGlobals = async () => {
  try {
    const siteGlobalsEndpoint = `${process.env.NEXT_PUBLIC_STRAPI_API_URL}/site-global`;

    const { data: siteGlobalsData } = await axios
      .get(siteGlobalsEndpoint)
      .catch((err) => console.log(`error fetching content for groups: `, err));

    return siteGlobalsData.data;
  } catch (err) {
    console.error("Failed to get data: ", err);
  }
};
