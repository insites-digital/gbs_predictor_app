import React from "react";
import styles from "./togglebutton.module.scss";
import cn from "classnames";

export const ToggleButton = ({ label, handleOnClick }) => {
  const slideClasses = cn(styles.toggleActive, styles.toggleSlide);
  return (
    <div className={styles.button}>
      <button
        onClick={() => {
          handleOnClick && handleOnClick();
        }}
      >
        {label && <span className={styles.label}>{label}</span>}
      </button>
      <span className={slideClasses} />
    </div>
  );
};
