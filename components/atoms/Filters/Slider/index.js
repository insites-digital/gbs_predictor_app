// ReactSlider
import { useContext, useEffect, useState } from "react";
import ReactSlider from "react-slider";
import { useVariable } from "hooks/useVariable";

// Styles
import styles from "./slider.module.scss";
import { ResetContext } from "pages/components/Variables";

export const SliderFilter = ({ item, children, ...props }) => {
  const { isReset } = useContext(ResetContext);
  const [value, setValue] = useState(item.parentWeight | item.childWeight);
  const { handleVariableChange } = useVariable();

  return (
    <div className={styles.container}>
      <ReactSlider
        className={styles.horizontalSlider}
        thumbClassName={styles.thumb}
        trackClassName={styles.track}
        value={value}
        onAfterChange={() => {
          handleVariableChange(value, item);
        }}
        max={item.parentRange || item.childRange}
        renderThumb={(props, state) => {
          isReset ? setValue(0) : setValue(state.valueNow);

          return (
            <>
              <span className={`${styles.value} span--accordionHeading`}>
                {value}
              </span>
              <div {...props} style={{ marginLeft: `${value * 9.5}%` }}></div>
            </>
          );
        }}
      />
    </div>
  );
};
