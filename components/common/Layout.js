import { Header, Loading } from "../";

export const Layout = ({ children }) => {
  return (
    <>
      <p className="landscape-text">Landscape orientations are unsupported</p>
      <div className="wrapper">
        <Header />
        <main className="main">{children}</main>
      </div>
    </>
  );
};
