import styles from "../../organisms/Sidebar/sidebar.module.scss";

export default function SidebarPanel({ children }) {
  return (
    <article className={styles.how_to_guide}>
      <div
        className={styles.how_to_guide_text}
        dangerouslySetInnerHTML={{ __html: children }}
      ></div>
    </article>
  );
}
