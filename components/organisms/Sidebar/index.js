import { useEffect, useState } from "react";

import styles from "./sidebar.module.scss";
import Image from "next/image";
import { useTournament } from "hooks/useTournament";
import Link from "../../atoms/Link";
import moment from "moment";
import SidebarPanel from "@/components/molecules/SidebarPanel";

const gbsLogo = "/images/gbs-logo.svg";

const twitterIcon = "/images/icons/twitter.svg";
const youtubeIcon = "/images/icons/youtube.svg";
const facebookIcon = "/images/icons/facebook.svg";

export const Sidebar = ({
  siteGlobalData,
  liveTournaments,
  activeTournament,
}) => {
  console.log("SIDEBAR :: ", liveTournaments);

  const { handleTournamentChange } = useTournament();

  // Mobile navigation handlers
  const prevTournament = (current) => {
    const currentTournament = liveTournaments.find(
      (tournament) => tournament.slug === current
    );

    return liveTournaments[liveTournaments.indexOf(currentTournament) - 1]
      ?.slug;
  };

  const nextTournament = (current) => {
    const currentTournament = liveTournaments.find(
      (tournament) => tournament.slug === current
    );

    const next =
      liveTournaments[liveTournaments.indexOf(currentTournament) + 1]?.slug;

    return next ? next : null;
  };

  const MobilePrevLink = ({ tournament, hasPrev }) => {
    console.log("asd", tournament);
    return (
      <Link
        url={`/tournament/${tournament?.attributes.slug}`}
        className={`${styles.prev} ${!hasPrev && styles.disable}`}
      >
        <div
          onClick={() => {
            prevTournament(activeTournament.slug) &&
              handleTournamentChange(prevTournament(activeTournament.slug));
          }}
        />
      </Link>
    );
  };

  const MobileNextLink = ({ tournament, hasNext }) => (
    <Link
      url={`/tournament/${tournament?.attributes.slug}`}
      className={`${styles.next} ${!hasNext && styles.disable}`}
    >
      <div
        onClick={() => {
          nextTournament(activeTournament.slug) &&
            handleTournamentChange(nextTournament(activeTournament.slug));
        }}
      />
    </Link>
  );

  const MobileActiveLink = () => {
    const {
      Tournament_Name,
      Tournament_Info,
      Tournament_Start,
      Tournament_End,
      updatedAt,
      Tour,
    } = activeTournament;

    return (
      <div className={styles.current}>
        <div className={styles.current_header}>
          {/* {isLive(activeTournament) && (
            <div className={styles.livePill}>
              <span>Live</span>
            </div>
          )} */}
          <h2 className={styles.sidebar_tournament_name}>{Tournament_Name}</h2>
        </div>
        <div className={styles.current_info}>
          <p className={styles.sidebar_updated}>
            <span className={styles.sidebar_tour_type_mobile}>{Tour}</span>
            {`Updated ${moment(updatedAt).format("h:mm a, DD/MM/YY")}`}
          </p>
          <p className={styles.sidebar_date}>{`${moment(
            Tournament_Start
          ).format("MMM Do")}-${moment(Tournament_End).format(
            "MMM Do"
          )}, ${moment(Tournament_End).format("YYYY")}`}</p>
          <p className={styles.sidebar_location}>{Tournament_Info}</p>
        </div>
      </div>
    );
  };

  return (
    <div>
      <div className={styles.mobileNav}>
        {/* Handle mobile navigation with less than three links */}
        {liveTournaments?.length == 1 && (
          <MobileActiveLink tournament={activeTournament} />
        )}
        {liveTournaments?.length < 3
          ? liveTournaments?.map((tournament, index) => {
              return index === 0 ? (
                liveTournaments?.length <= 1 ? null : (
                  <MobilePrevLink
                    key={tournament.id}
                    tournament={liveTournaments[index]}
                    hasPrev={!!prevTournament(activeTournament)}
                  />
                )
              ) : (
                <>
                  <div key={tournament.id}>
                    <MobileActiveLink tournament={tournament} />
                  </div>

                  {liveTournaments?.length <= 1 ? null : (
                    <MobileNextLink
                      tournament={liveTournaments[index]}
                      hasNext={!!nextTournament(activeTournament)}
                    />
                  )}
                </>
              );
            })
          : liveTournaments?.map((tournament, index) => {
              return index === 0 ? (
                <MobilePrevLink
                  key={tournament.id}
                  tournament={liveTournaments[index]}
                  hasPrev={!!prevTournament(activeTournament)}
                />
              ) : index === liveTournaments.length - 1 ? (
                <MobileNextLink
                  key={tournament.id}
                  tournament={liveTournaments[index]}
                  hasNext={!!nextTournament(activeTournament)}
                />
              ) : (
                <MobileActiveLink key={tournament.id} tournament={tournament} />
              );
            })}
      </div>

      <div
        className={`${styles.sidebar} ${
          liveTournaments?.length === 1
            ? styles.sidebar_1_tab
            : liveTournaments?.length === 2
            ? styles.sidebar_2_tabs
            : ""
        }`}
      >
        <aside className={styles.sidebar_inner}>
          {liveTournaments?.map((tournament, index) => {
            const {
              Tour,
              Tournament_Name,
              Tournament_Icon,
              Tournament_Start,
              Tournament_End,
              Tournament_Info,
              updatedAt,
              slug,
              Live,
            } = tournament.attributes;

            return (
              <Link
                key={index}
                // onClick={() =>
                //   handleTournamentChange(slug)
                // }
                url={`/tournament/${slug}`}
                className={`${styles.sidebar_tab} ${
                  activeTournament.id === tournament.id ? styles.active : ""
                }`}
              >
                <div>
                  <div className={styles.sidebar_tab_left}>
                    <div className={styles.sidebar_logo}>
                      {Tournament_Icon?.data && (
                        <Image
                          src={`${process.env.NEXT_PUBLIC_IMAGE_DOMAINS}${Tournament_Icon?.data?.attributes?.url}`}
                          alt={Tournament_Icon?.data?.attributes?.alt}
                          width={50}
                          height={50}
                          objectFit="contain"
                        />
                      )}
                    </div>
                    {/* {Live && (
                        <div className={styles.livePill}>
                          <span>Live</span>
                        </div>
                      )} */}
                  </div>

                  <div className={styles.sidebar_tab_right}>
                    <p className={styles.sidebar_tour_type_desktop}>{Tour}</p>
                    <h2 className={styles.sidebar_tournament_name}>
                      {Tournament_Name}
                    </h2>

                    <div
                      className={
                        activeTournament !== tournament.id && styles.hideInfo
                      }
                    >
                      <p className={styles.sidebar_updated}>
                        <span className={styles.sidebar_tour_type_mobile}>
                          {Tour}
                        </span>
                        {`Updated ${moment(updatedAt).format(
                          "h:mm a, DD/MM/YY"
                        )}`}
                      </p>
                      <p className={styles.sidebar_date}>
                        {`${moment(Tournament_Start).format("MMM Do")}-${moment(
                          Tournament_End
                        ).format("MMM Do")}, ${moment(Tournament_End).format(
                          "YYYY"
                        )}`}
                      </p>
                      <p className={styles.sidebar_location}>
                        {Tournament_Info}
                      </p>
                    </div>
                  </div>
                </div>
              </Link>
            );
          })}
          <SidebarPanel>{siteGlobalData}</SidebarPanel>
          <div className={styles.sidebar_socials}>
            <div className={styles.sidebar_golf_logo}>
              <a href="https://www.golfbettingsystem.co.uk">
                <Image src={gbsLogo} alt="logo" width={70} height={55} />
              </a>
            </div>

            <div className={styles.sidebar_social_links}>
              <a href="https://twitter.com/GolfBetting">
                <Image src={twitterIcon} alt="twitter" width={32} height={32} />
              </a>

              <a href="https://www.youtube.com/channel/UCeMmMf77Rn05-WPP6-Alakw">
                <Image src={youtubeIcon} alt="youtube" width={32} height={32} />
              </a>

              <a href="https://www.facebook.com/groups/254181393361/">
                <Image
                  src={facebookIcon}
                  alt="facebook"
                  width={32}
                  height={32}
                />
              </a>
            </div>
          </div>
        </aside>
      </div>
    </div>
  );
};
