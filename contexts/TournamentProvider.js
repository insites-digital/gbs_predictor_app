import { createContext, useState, useEffect } from "react";
import {
  queryImportedTournament,
  queryAllImportedTournaments,
} from "../lib/wordpress/posts/getImportedData";

import { useQuery } from "react-query";

export const TournamentContext = createContext({
  tournament: {},
  allTournaments: [],
});

export const TournamentProvider = ({ value, children }) => {
  const [activeTournament, setActiveTournament] = useState(
    value.tournament.slug
  );

  useEffect(() => {
    setActiveTournament(value.tournament.slug);
  }, [value.tournament.slug]);

  // Get all tournaments
  // const { data: allTournaments, isLoading: isLoadingTournaments } = useQuery(
  //   ["importedTournaments"],
  //   () => queryAllImportedTournaments()
  // );

  // Get tournament based on ID
  const { data: tournament } = useQuery(
    ["importedTournament", activeTournament],
    () => queryImportedTournament(activeTournament)
  );

  const state = {
    tournament,
    activeTournament,
    setActiveTournament,
  };

  return (
    <TournamentContext.Provider value={state}>
      {typeof children === "function" ? children(state) : children}
    </TournamentContext.Provider>
  );
};
