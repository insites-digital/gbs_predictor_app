import React, { useContext, useEffect } from "react";
import PropTypes from "prop-types";
import { DefaultSeo } from "next-seo";
import { useRouter } from "next/router";
import * as ga from "../lib/google/analytics";
import {
  TournamentProvider,
  TournamentContext,
} from "contexts/TournamentProvider";
import { QueryClient, QueryClientProvider } from "react-query";
import "../styles/app.scss";

const WebApp = ({ Component, pageProps }) => {
  const { isLoadingTournaments } = useContext(TournamentContext);

  const router = useRouter();

  useEffect(() => {
    const handleRouteChange = (url) => {
      ga.pageview(url);
    };
    //When the component is mounted, subscribe to router changes
    //and log those page views
    router.events.on("routeChangeComplete", handleRouteChange);

    // If the component is unmounted, unsubscribe
    // from the event with the `off` method
    return () => {
      router.events.off("routeChangeComplete", handleRouteChange);
    };
  }, [router.events]);

  const defaultSeoData = {
    title: undefined,
    titleTemplate: `%s | ${process.env.NEXT_PUBLIC_SITE_NAME}`,
    defaultTitle: process.env.NEXT_PUBLIC_SITE_NAME,
  };

  const twentyFourHoursInMs = 1000 * 60 * 60 * 24;
  const queryClient = new QueryClient({
    defaultOptions: {
      queries: {
        refetchOnWindowFocus: false,
        refetchOnmount: false,
        refetchOnReconnect: false,
        retry: false,
        staleTime: twentyFourHoursInMs,
      },
    },
  });

  return (
    <QueryClientProvider client={queryClient}>
      {isLoadingTournaments ? (
        <></>
      ) : (
        // <TournamentProvider>
        <>
          {!!defaultSeoData && <DefaultSeo {...defaultSeoData} />}
          <Component {...pageProps} />
        </>
        // {/* </TournamentProvider> */}
      )}
    </QueryClientProvider>
  );
};

export default WebApp;

WebApp.propTypes = {
  Component: PropTypes.any.isRequired,
  pageProps: PropTypes.object.isRequired,
};
