// Components
import { Panel, Accordion, Table, Heading } from "../../../components";

// Contexts
import { useVariable } from "hooks/useVariable";

// Styles
import styles from "./variables.module.scss";
import { Loading } from "@/components/atoms/Loading";
import { ToggleButton } from "@/components/atoms/ToggleButton";
import { createContext, useContext, useEffect, useState } from "react";
import { usePlayers } from "hooks/usePlayers";

export const ResetContext = createContext({});

const Variables = ({ hideHeading }) => {
  const { groups, handleVariableChange, setOpenFilters, openFilters } =
    useVariable();
  const activeVariable = groups?.find((group) => group.active);
  const [isReset, setIsReset] = useState(false);

  const { resetFilters } = usePlayers();

  return (
    <div className="section_left">
      <div className="header">
        <Heading
          styles="h1--primary"
          className={`h1--primary ${hideHeading && "hideHeading"}`}
        >
          Variables
        </Heading>
        <ToggleButton
          label={"Reset"}
          handleOnClick={() => {
            handleVariableChange(0);
            resetFilters();
            setOpenFilters([...openFilters]);
            setIsReset(true);
            setTimeout(() => {
              setIsReset(false);
            }, 400);
          }}
        />
      </div>
      {!groups?.length > 0 ? (
        <Loading />
      ) : (
        <div className="panel panel_left">
          <Panel>
            <div>
              <ResetContext.Provider value={{ isReset, setIsReset }}>
                <Accordion singular={true} initial={activeVariable?.id || 1}>
                  {groups?.map((groupItem) => {
                    return (
                      <>
                        <Accordion.Item
                          key={groupItem.id}
                          label={groupItem.parentName}
                          item={groupItem}
                          slider={true}
                          active={groupItem.active}
                        >
                          <Table>
                            <Table.Body>
                              {groupItem.childVariables?.map((variableItem) => {
                                return (
                                  <Table.Row
                                    key={variableItem.id}
                                    className={styles.row}
                                  >
                                    <Table.Cell className={styles.cell}>
                                      <span className="span--accordionItem">
                                        {variableItem.Child_Name}
                                      </span>
                                    </Table.Cell>
                                    <Table.Cell.Slider item={variableItem} />
                                  </Table.Row>
                                );
                              })}
                            </Table.Body>
                          </Table>
                        </Accordion.Item>
                      </>
                    );
                  })}
                </Accordion>
              </ResetContext.Provider>
            </div>
          </Panel>
        </div>
      )}
    </div>
  );
};

export default Variables;
