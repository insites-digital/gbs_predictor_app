import { useState, useEffect } from "react";
import Variables from "../components/Variables";
import Results from "../components/Results";
import Image from "next/image";
import Page from "../../components/common/Page";
import { Sidebar, Footer } from "../../components/";
import { VariableProvider } from "contexts/VariableProvider";
import { PlayerProvider } from "contexts/PlayerProvider";
import { TournamentProvider } from "contexts/TournamentProvider";

import {
  queryAllImportedTournaments,
  queryImportedTournament,
} from "../../lib/wordpress/posts/getImportedData.js";
import { querySiteGlobals } from "../../lib/wordpress/global/getGlobalContent.js";

const TournamentPage = ({ siteGlobalData, tournament, liveTournaments }) => {
  const [activeContent, setActiveContent] = useState("results");
  console.log("Site Globals :: ", siteGlobalData);
  console.log("TOURNAMENT :: ", tournament);
  console.log("LIVE TOURNAMENT :: ", liveTournaments);

  if (!tournament) return <></>;

  // handleTournamentChange(tournament.slug);

  // useEffect(() => {
  //   handleTournamentChange(tournament.slug);
  // }, [tournament.slug]);

  // useEffect(() => {
  //   return () => tournament && handleTournamentChange(tournament.slug);
  // }, [tournament]);

  return (
    <Page seo={{ title: `${tournament.Tournament_Name} | ${tournament.Tour}` }}>
      {liveTournaments && tournament && (
        <Sidebar
          siteGlobalData={siteGlobalData.attributes.Sidebar_info}
          liveTournaments={liveTournaments}
          activeTournament={tournament}
        />
      )}
      <TournamentProvider value={{ tournament }}>
        <section className="main-section">
          <div>
            <div className="content-toggle">
              <Image
                src={
                  activeContent === "variables"
                    ? "/images/variablesOpen.svg"
                    : "/images/variablesClosed.svg"
                }
                alt="bookmaker"
                width={24}
                height={21}
                onClick={() =>
                  setActiveContent((prev) =>
                    prev === "results" ? "variables" : "results"
                  )
                }
              />
            </div>
            <VariableProvider>
              <div
                className={`section_left ${
                  activeContent === "variables"
                    ? "showVariables"
                    : "hideVariables"
                }`}
              >
                <Variables hideHeading={true} />
              </div>

              <PlayerProvider>
                <Results show={activeContent === "results"} />
              </PlayerProvider>
            </VariableProvider>
          </div>
          <Footer
            siteGlobalData={siteGlobalData.attributes.footer_smallprint}
          />
        </section>
      </TournamentProvider>
    </Page>
  );
};

export async function getServerSideProps({ params }) {
  const siteGlobalData = await querySiteGlobals();
  const getAllTournaments = queryAllImportedTournaments();
  const getSingleTournament = queryImportedTournament(params.slug);

  const [allTournaments, tournament] = await Promise.all([
    getAllTournaments,
    getSingleTournament,
  ]);

  const liveTournaments = allTournaments?.filter(
    ({ attributes }) =>
      attributes.Live &&
      attributes.Imported_Variables.length > 0 &&
      attributes.Player_List.data.length > 0
  );

  return {
    props: { tournament, liveTournaments, siteGlobalData },
  };
}

// export async function getStaticPaths() {
//   const tournaments = await queryAllImportedTournaments();

//   const paths = tournaments?.length
//     ? tournaments.map((tournament) => {
//         const { id, Tournament_Name } = tournament.attributes;
//         return {
//           params: {
//             slug: Tournament_Name.replace(/\s+/g, "-").toLowerCase(),
//             id,
//           },
//         };
//       })
//     : [];

//   return {
//     paths,
//     fallback: false,
//   };
// }

export default TournamentPage;
