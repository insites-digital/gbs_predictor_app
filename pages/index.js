// Components
import { queryAllImportedTournaments } from "../lib/wordpress/posts/getImportedData";
import Page from "../components/common/Page";
import { useTournament } from "../hooks/useTournament";

const Home = ({
  liveTournaments,
  defaultTournament,
  defaultTournamentPath,
}) => {
  console.log("LIVE TOURNAMENTS :: ", liveTournaments);
  console.log("DEFAULT TOURNAMENT :: ", defaultTournament);
  console.log("DEFAULT TOURNAMENT PATH :: ", defaultTournamentPath);

  return <Page seo={{ title: "GBS Predictor" }} />;
};

export default Home;

export async function getServerSideProps() {
  const getAllTournaments = queryAllImportedTournaments();

  const [allTournaments] = await Promise.all([getAllTournaments]);

  const liveTournaments = allTournaments?.filter(
    ({ attributes }) =>
      attributes.Live &&
      attributes.Imported_Variables.length > 0 &&
      attributes.Player_List.data.length > 0
  );

  const defaultTournament = allTournaments?.find(
    (tournament) => tournament.attributes.Live
  );

  const defaultTournamentPath = `/tournament/${defaultTournament?.attributes.Tournament_Name.replace(
    /\s+/g,
    "-"
  ).toLowerCase()}`;

  if (
    allTournaments &&
    liveTournaments &&
    defaultTournament &&
    defaultTournamentPath
  ) {
    return {
      redirect: {
        destination: defaultTournamentPath,
      },
    };
  }

  return {
    props: { liveTournaments, defaultTournament, defaultTournamentPath },
  };
}
